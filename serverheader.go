package echomiddleware

import (
	"github.com/labstack/echo/v4"
)

// ServerHeader middleware adds a `Server` header to the response.
func ServerVersionHeader(servername, version string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Response().Header().Set(echo.HeaderServer, servername)
			c.Response().Header().Set("Server-Version", version)
			return next(c)
		}
	}
}
