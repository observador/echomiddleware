package echomiddleware

import (
	"fmt"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	httpRequestCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_request_count",
			Help: "Number of Published Messages Rests.",
		}, []string{"path", "method", "status"})

	httpRequestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_request_duration_seconds",
			Help:    "Http request duration in seconds",
			Buckets: prometheus.DefBuckets,
		}, []string{"path", "method"})
)

func init() {
	prometheus.MustRegister(httpRequestCount)
	prometheus.MustRegister(httpRequestDuration)
}

func Prometheus(ignore Ignore) func(next echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if ignore(c) {
				return next(c)
			}

			start := time.Now()
			defer func() {
				if err := recover(); err != nil {
					httpRequestCount.With(prometheus.Labels{
						"path":   c.Path(),
						"method": c.Request().Method,
						"status": "500",
					}).Inc()
					httpRequestDuration.With(prometheus.Labels{
						"path":   c.Path(),
						"method": c.Request().Method,
					}).Observe(time.Since(start).Seconds())

					panic(err)
				}
			}()

			err := next(c)
			path := c.Path()
			if err != nil {
				c.Error(err)
				if c.Response().Status == 404 {
					path = "/notfound"
				}
			}

			httpRequestCount.With(prometheus.Labels{
				"path":   path,
				"method": c.Request().Method,
				"status": fmt.Sprintf("%d", c.Response().Status),
			}).Inc()

			httpRequestDuration.With(prometheus.Labels{
				"path":   path,
				"method": c.Request().Method,
			}).Observe(time.Since(start).Seconds())

			return err
		}
	}
}

// Observe records an HTTP request
func Observe(method, path string, status int, dur time.Duration) {
	httpRequestCount.With(prometheus.Labels{
		"path":   path,
		"method": method,
		"status": fmt.Sprintf("%d", status),
	}).Inc()

	httpRequestDuration.With(prometheus.Labels{
		"path":   path,
		"method": method,
	}).Observe(dur.Seconds())
}
