module bitbucket.org/observador/echomiddleware/v3

go 1.13

require (
	bitbucket.org/observador/obs-utils/v2 v2.0.8
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/labstack/echo/v4 v4.1.10
	github.com/prometheus/client_golang v1.6.0
)
