package echomiddleware

import (
	"fmt"
	"github.com/labstack/echo/v4"
)

func RedirectWithConfig(code int, host string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			req, scheme := c.Request(), c.Scheme()
			url := fmt.Sprintf("%s://%s%s",scheme,host,req.RequestURI)
			return c.Redirect(code, url)
		}
	}
}
