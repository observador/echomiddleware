package echomiddleware

import "github.com/labstack/echo/v4"

type Ignore func(c echo.Context) bool
